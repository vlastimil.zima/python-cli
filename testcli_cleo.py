#!/usr/bin/python3
"""Test client interface."""
import json
from typing import cast
from datetime import date, datetime

from cleo import Application, Command

import testcli


class BaseCommand(Command):
    """Base command."""

    func: staticmethod

    def handle(self):
        config = testcli.DEFAULT_CONFIG.copy()
        if self.option('config'):
            with open(self.option('config')) as config_file:
                config.update(json.loads(config_file.read()))

        string = cast(str, self.option('string') or config['string'])
        if self.option('number'):
            number = int(self.option('number'))
        else:
            number = cast(int, config['number'])
        if self.option('params'):
            params = dict(p.split('=', 1) for p in self.option('params'))
        else:
            params = cast(dict, config['params'])
        if self.option('today') == 'today':
            today = date.today()
        else:
            today = datetime.strptime(self.option('today'), '%Y-%m-%d').date()
        choices = self.option('choices')
        if testcli.Choice.ALL in choices or not choices:
            choices = [c for c in testcli.Choice if c != testcli.Choice.ALL]
        else:
            if not set(choices).issubset(set(testcli.Choice)):
                raise ValueError('Unknown choice')
            choices = [testcli.Choice(c) for c in choices]

        self.func(self.argument('label'), string, number, params, today, choices, self.option('dry-run'),
                  self.io.verbosity, stdout=self.io)


class CreateCommand(BaseCommand):
    """Creates

    create
        {label : A label}
        {--config= : Set config file.}
        {--string= : Set custom string.}
        {--number= : Set custom number.}
        {--params=* : Set custom parameters.}
        {--today=today : Set custom today.}
        {--choices=* : Set custom choices. Available options: 'all', 'foo', 'bar', 'baz'. (default: all)}
        {--dry-run : Don't actually do anything.}
    """

    func = staticmethod(testcli.create)


class DropCommand(BaseCommand):
    """Drops

    drop
        {label : A label}
        {--config= : Set config file.}
        {--string= : Set custom string.}
        {--number= : Set custom number.}
        {--params=* : Set custom parameters.}
        {--today=today : Set custom today.}
        {--choices=* : Set custom choices. Available options: 'all', 'foo', 'bar', 'baz'. (default: all)}
        {--dry-run : Don't actually do anything.}
    """

    func = staticmethod(testcli.drop)


APPLICATION = Application(name="testcli_cleo", version=testcli.__version__)
APPLICATION.add(CreateCommand())
APPLICATION.add(DropCommand())


if __name__ == '__main__':
    APPLICATION.run()
