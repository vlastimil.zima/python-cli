import shlex
from abc import ABC, abstractmethod
from datetime import date
from typing import Callable, Sequence, Type, cast
from unittest import TestCase, skip

import plac
from cleo import ApplicationTester
from click.testing import CliRunner, Result
from testfixtures import OutputCapture, StringComparison
from typer.testing import CliRunner as TyperCliRunner

import testcli
import testcli_argparse
import testcli_cleo
import testcli_click
import testcli_docopt
import testcli_plac
import testcli_typer


class TestCliMixin(ABC):
    default_verbosity = 1
    help_args: Sequence[Sequence[str]] = (
        ['--help'],
        ['-h'],
    )
    very_verbose_value = 3
    very_verbose_args: Sequence[Sequence[str]] = (
        ['--verbosity', '3'],
        ['-v', '3'],
    )
    dry_run_args: Sequence[Sequence[str]] = (
        ['--dry-run'],
        ['-n'],
    )

    @abstractmethod
    def call_command(self, args: Sequence[str]) -> Result:
        pass

    def assertOutputLine(self, output: str, line):
        out_lines = tuple(l.strip() for l in output.split('\n'))
        self.assertIn(line, out_lines)

    def assertOutputContains(self, output: str, needle):
        self.assertIn(needle, output)

    def test_help(self):
        for args in self.help_args:
            with self.subTest(args=args):
                result = self.call_command(['create', 'example'] + args)
                self.assertEqual(result.exit_code, 0)
                self.assertOutputContains(result.output, '--help')

    def test_version(self):
        result = self.call_command(['--version'])
        self.assertEqual(result.exit_code, 0)
        self.assertOutputContains(result.output, testcli.__version__)

    @abstractmethod
    def assertUsage(self, result: Result) -> None:
        pass

    def _test_error(self, argv: Sequence[str]) -> None:
        result = self.call_command(argv)
        self.assertNotEqual(result.exit_code, 0)
        self.assertUsage(result)

    def test_unknown_command(self):
        self._test_error(['unknown'])

    def test_create(self):
        result = self.call_command(['create', 'example'])
        self.assertOutputLine(result.output, 'Command: create')
        self.assertOutputLine(result.output, 'Label: example')

    def test_drop(self):
        result = self.call_command(['drop', 'example'])
        self.assertOutputLine(result.output, 'Command: drop')
        self.assertOutputLine(result.output, 'Label: example')

    def test_default_options(self):
        result = self.call_command(['create', 'example'])
        self.assertOutputLine(result.output, "String: Don't panic!")
        self.assertOutputLine(result.output, 'Number: 42')
        self.assertOutputLine(result.output, "Params: {}")
        self.assertOutputLine(result.output, "Today: {}".format(date.today()))
        self.assertOutputLine(result.output, "Choices selected: ['foo', 'bar', 'baz']")
        self.assertOutputLine(result.output, "Dry run: False")
        self.assertOutputLine(result.output, "Verbosity: {}".format(self.default_verbosity))

    def test_config(self):
        result = self.call_command(['create', 'example', '--config', 'test_config.json'])
        self.assertOutputLine(result.output, "String: Hitchhiker's guide")
        self.assertOutputLine(result.output, 'Number: 17')
        self.assertOutputLine(result.output, "Params: {'file': True}")

    def test_string(self):
        result = self.call_command(['create', 'example', '--string', 'Arthur Dent'])
        self.assertOutputLine(result.output, "String: Arthur Dent")

    def test_number(self):
        result = self.call_command(['create', 'example', '--number', '255'])
        self.assertOutputLine(result.output, "Number: 255")

    def test_number_invalid(self):
        self._test_error(['create', 'example', '--number', 'five'])

    def test_params(self):
        result = self.call_command(['create', 'example', '--params', 'name=Arthur', '--params', 'last=Dent'])
        self.assertOutputLine(result.output, "Params: {'name': 'Arthur', 'last': 'Dent'}")

    def test_today(self):
        result = self.call_command(['create', 'example', '--today', '2020-05-25'])
        self.assertOutputLine(result.output, "Today: 2020-05-25")

    def test_today_not_date(self):
        self._test_error(['create', 'example', '--today', 'yesterday'])

    def test_choices_all(self):
        result = self.call_command(['create', 'example', '--choices', 'all'])
        self.assertOutputLine(result.output, "Choices selected: ['foo', 'bar', 'baz']")

    def test_choices_one(self):
        result = self.call_command(['create', 'example', '--choices', 'foo'])
        self.assertOutputLine(result.output, "Choices selected: ['foo']")

    def test_choices_multi(self):
        result = self.call_command(['create', 'example', '--choices', 'bar', '--choices', 'baz'])
        self.assertOutputLine(result.output, "Choices selected: ['bar', 'baz']")

    def test_choices_invalid(self):
        self._test_error(['create', 'example', '--choices', 'invalid'])

    def test_choices_ambiguos(self):
        # Test --choice between command and label which may cause ambiguity.
        result = self.call_command(['create', '--choices', 'foo', 'bar'])
        self.assertOutputLine(result.output, 'Command: create')
        self.assertOutputLine(result.output, 'Label: bar')
        self.assertOutputLine(result.output, "Choices selected: ['foo']")

    def test_dry_run(self):
        for args in self.dry_run_args:
            with self.subTest(args=args):
                result = self.call_command(['create', 'example'] + args)
                self.assertOutputLine(result.output, "Dry run: True")

    def test_verbosity(self):
        for args in self.very_verbose_args:
            with self.subTest(args=args):
                result = self.call_command(['create', 'example'] + args)
                self.assertOutputLine(result.output, "Verbosity: {}".format(self.very_verbose_value))


class DummyRunner:
    charset = 'utf-8'


class ArgparseTest(TestCliMixin, TestCase):
    def call_command(self, args: Sequence[str]) -> Result:
        code = 0
        exception = None
        try:
            with OutputCapture(separate=True) as output:
                testcli_argparse.main(args)
        except SystemExit as error:
            if error.code is not None:
                code = error.code
            exception = error

        return Result(DummyRunner, output.stdout.getvalue().encode(), output.stderr.getvalue().encode(), code,
                      exception)

    def assertUsage(self, result: Result) -> None:
        self.assertIn('usage:', result.stderr)


class CleoTest(TestCliMixin, TestCase):
    default_verbosity = 0
    very_verbose_value = 4
    very_verbose_args = (
        # --verbose doesn't seem to work
        ['-vvv'],
    )
    dry_run_args = (
        ['--dry-run'],
    )

    def call_command(self, args: Sequence[str]) -> Result:
        tester = ApplicationTester(testcli_cleo.APPLICATION)
        tester.execute(shlex.join(args))
        return Result(DummyRunner, tester.io.output.stream.fetch().encode(),
                      tester.io.error_output.stream.fetch().encode(),
                      tester.status_code, None)

    def assertUsage(self, result: Result) -> None:
        self.assertTrue(len(result.stdout))


class ClickTest(TestCliMixin, TestCase):
    def call_command(self, args: Sequence[str]) -> Result:
        runner = CliRunner()
        return runner.invoke(testcli_click.main, args)

    def assertUsage(self, result: Result) -> None:
        self.assertIn('Usage:', result.stdout)


class DocoptTest(TestCliMixin, TestCase):
    def call_command(self, args: Sequence[str]) -> Result:
        code = 0
        exception = None
        try:
            with OutputCapture(separate=True) as output:
                testcli_docopt.main(args)
        except SystemExit as error:
            if error.code is not None:
                code = error.code
            exception = error

        return Result(DummyRunner, output.stdout.getvalue().encode(), output.stderr.getvalue().encode(), code,
                      exception)

    def assertUsage(self, result: Result) -> None:
        self.assertIn('--help', cast(str, result.exception.code))
        self.assertEqual(result.stdout, '')
        self.assertEqual(result.stderr, '')


class PlacTest(TestCliMixin, TestCase):
    very_verbose_args: Sequence[Sequence[str]] = (
        ['--verbosity', '3'],
        # -v is used for version
    )

    @classmethod
    def setUpClass(cls):
        #XXX: Copied from plac_core.call
        cls.parser = plac.parser_from(testcli_plac.main)
        cls.parser.add_argument('--version', '-v', action='version', version=testcli.__version__)

    def call_command(self, args: Sequence[str]) -> Result:
        code = 0
        exception = None
        try:
            with OutputCapture(separate=True) as output:
                self.parser.consume(args)
        except SystemExit as error:
            if error.code is not None:
                code = error.code
            exception = error

        return Result(DummyRunner, output.stdout.getvalue().encode(), output.stderr.getvalue().encode(), code,
                      exception)

    def assertUsage(self, result: Result) -> None:
        self.assertIn('usage:', result.stderr)

    @skip("Plac doesn't allow options to be used multiple times.")
    def test_params(self):
        super().test_params()

    @skip("Plac doesn't allow options to be used multiple times.")
    def test_choices_multi(self):
        super().test_choices_multi()


class TyperTest(TestCliMixin, TestCase):
    help_args = (
        ['--help'],
    )

    def call_command(self, args: Sequence[str]) -> Result:
        runner = TyperCliRunner()
        return runner.invoke(testcli_typer.APPLICATION, args)

    def assertUsage(self, result: Result) -> None:
        self.assertIn('Usage:', result.stdout)

    @skip("Typer doesn't provide --version at application level.")
    def test_version(self):
        super().test_version()
