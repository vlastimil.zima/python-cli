#!/usr/bin/python3
"""Test client interface."""
import argparse
import json
from datetime import date, datetime
from enum import Enum, unique
from typing import List, Tuple, cast

import plac

import testcli

ALL_CHOICES = ['foo', 'bar', 'baz']


def parse_date(value: str):
    return datetime.strptime(value, '%Y-%m-%d').date()


def parse_params(value: str):
    return value.split('=', 1)


@plac.pos('command', "Command to execute", choices=['create', 'drop'])
@plac.opt('config', "Set config file.", type=argparse.FileType('r'))
@plac.opt('string', "Set custom string.")
@plac.opt('number', "Set custom number.", type=int, abbrev='number')
@plac.opt('params', "Set custom parameters.", type=parse_params)
@plac.opt('today', "Set custom today.", type=parse_date)
@plac.opt('choices', "Set custom choices. Available options: 'all', 'foo', 'bar', 'baz'.", type=testcli.Choice,
          choices=list(testcli.Choice), abbrev='choices')
@plac.flg('dry_run', "Don't actually do anything.", abbrev='n')
@plac.opt('verbosity', "Set verbosity level in range 0 to 3 [default: 1].", type=int, abbrev='verbosity')
@plac.annotations()
def main(command: str,
         label: str,
         config: argparse.FileType = None,
         string: str = None,
         number: int = None,
         params: List[Tuple[str, str]] = None,
         today: date = date.today(),
         choices: testcli.Choice = testcli.Choice.ALL.value,
         dry_run: bool = False,
         verbosity: int = 1):

    defaults = testcli.DEFAULT_CONFIG.copy()
    if config:
        defaults.update(json.loads(config.read()))
    string = string or cast(str, defaults['string'])
    number = number or cast(int, defaults['number'])
    if params:
        parsed_params = dict([params])
    else:
        parsed_params = defaults.get('params')

    if choices == testcli.Choice.ALL:
        choices_list = [c for c in testcli.Choice if c != testcli.Choice.ALL]
    else:
        # Turn the choice into a list.
        choices_list = [choices]

    if command == 'create':
        testcli.create(label, string, number, parsed_params, today, choices_list, dry_run, verbosity)
    else:
        assert command == 'drop'
        testcli.drop(label, string, number, parsed_params, today, choices_list, dry_run, verbosity)


if __name__ == '__main__':
    plac.call(main, version=testcli.__version__)
