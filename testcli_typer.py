#!/usr/bin/python3
"""Test client interface."""
import json
from datetime import date, datetime
from enum import Enum, unique
from typing import List, Optional, cast

import testcli
import typer

APPLICATION = typer.Typer()


def version_callback(value: bool):
    if value:
        typer.echo("Version: {}".format(testcli.__version__))
        raise typer.Exit()


@APPLICATION.command()
def create(
        label: str,
        config: typer.FileText = typer.Option(None, help="Set config file."),
        string: str = typer.Option(None, help="Set custom string."),
        number: int = typer.Option(None, help="Set custom number."),
        params: List[str] = typer.Option(None, help="Set custom parameters."),
        today: datetime = typer.Option(str(date.today()), formats=['%Y-%m-%d'], help="Set custom today.  [default: today]", show_default=False),
        choices: List[testcli.Choice] = typer.Option([testcli.Choice.ALL.value], help="Set custom choices."),
        dry_run: bool = typer.Option(False, '-n', '--dry-run', help="Don't actually do anything."),
        verbosity: int = typer.Option(1, '-v', '--verbosity', help="Set verbosity level in range 0 to 3."),
        version: Optional[bool] = typer.Option(None, "--version", callback=version_callback, is_eager=True, help="Show version and exit.")):
    defaults = testcli.DEFAULT_CONFIG.copy()
    if config:
        defaults.update(json.loads(config.read()))
    string = string or cast(str, defaults['string'])
    number = number or cast(int, defaults['number'])
    if params:
        parsed_params = dict(p.split('=', 1) for p in params)  # type: ignore[arg-type]
    else:
        parsed_params = cast(dict, defaults['params'])

    if testcli.Choice.ALL in choices:
        choices = [c for c in testcli.Choice if c != testcli.Choice.ALL]

    testcli.create(label, string, number, parsed_params, today.date(), choices, dry_run, verbosity)


@APPLICATION.command()
def drop(
        label: str,
        config: typer.FileText = typer.Option(None, help="Set config file."),
        string: str = typer.Option(None, help="Set custom string."),
        number: int = typer.Option(None, help="Set custom number."),
        params: List[str] = typer.Option(None, help="Set custom parameters."),
        today: datetime = typer.Option(str(date.today()), formats=['%Y-%m-%d'], help="Set custom today.  [default: today]"),
        choices: List[testcli.Choice] = typer.Option([testcli.Choice.ALL.value], help="Set custom choices."),
        dry_run: bool = typer.Option(False, '-n', '--dry-run', help="Don't actually do anything."),
        verbosity: int = typer.Option(1, '-v', '--verbosity', help="Set verbosity level in range 0 to 3."),
        version: Optional[bool] = typer.Option(None, "--version", callback=version_callback, is_eager=True, help="Show version and exit.")):
    defaults = testcli.DEFAULT_CONFIG.copy()
    if config:
        defaults.update(json.loads(config.read()))
    string = string or cast(str, defaults['string'])
    number = number or cast(int, defaults['number'])
    if params:
        parsed_params = dict(p.split('=', 1) for p in params)  # type: ignore[arg-type]
    else:
        parsed_params = cast(dict, defaults['params'])

    if testcli.Choice.ALL in choices:
        choices = [c for c in testcli.Choice if c != testcli.Choice.ALL]

    testcli.drop(label, string, number, parsed_params, today.date(), choices, dry_run, verbosity)


if __name__ == '__main__':
    APPLICATION()
