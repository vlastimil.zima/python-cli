#!/usr/bin/python3
"""Test client interface.

Usage: testcli [options] [--choices=VALUE]... [--params=<KEY=VALUE>...] create <label>
       testcli [options] [--choices=VALUE]... [--params=<KEY=VALUE>...] drop <label>
       testcli -h | --help
       testcli --version

Options:
  -h, --help             Show this help message and exit.
  --version              Show program's version number and exit.
  --config=FILE          Set config file.
  --string=STRING        Set custom string.
  --number=NUMBER        Set custom number.
  --params=KEY=VALUE     Set custom parameters.
  --today=DATE           Set custom today [default: today].
  --choices=VALUE        Set custom choices [default: all]. Available options: 'all', 'foo', 'bar', 'baz'.
  -n, --dry-run          Don't actually do anything.
  -v,--verbosity=LEVEL   Set verbosity level in range 0 to 3 [default: 1].
"""
import itertools
import json
from datetime import date, datetime
from typing import Sequence, cast

from docopt import docopt, DocoptExit

import testcli


def main(argv: Sequence[str] = None):
    options = docopt(__doc__, argv=argv, version=testcli.__version__)

    try:
        config = testcli.DEFAULT_CONFIG.copy()
        if options['--config']:
            with open(options['--config']) as config_file:
                config.update(json.loads(config_file.read()))

        string = cast(str, options['--string'] or config['string'])
        if options['--number']:
            number = int(options['--number'])
        else:
            number = cast(int, config['number'])
        if options['--params']:
            params = dict(p.split('=', 1) for p in options['--params'])
        else:
            params = cast(dict, config['params'])
        if options['--today'] == 'today':
            today = date.today()
        else:
            today = datetime.strptime(options['--today'], '%Y-%m-%d').date()

        if testcli.Choice.ALL in options['--choices']:
            choices = [c for c in testcli.Choice if c != testcli.Choice.ALL]
        else:
            # Avoid bug https://github.com/docopt/docopt/issues/134
            choices = [i[0] for i in itertools.groupby(options['--choices'])]
            if not set(choices).issubset(set(testcli.Choice)):
                raise ValueError('Unknown choice')
            choices = [testcli.Choice(c) for c in choices]
    except ValueError as error:
        raise DocoptExit(str(error))

    if options['create']:
        testcli.create(options['<label>'], string, number, params, today, choices, options['--dry-run'],
                       int(options['--verbosity']))
    else:
        assert options['drop']
        testcli.drop(options['<label>'], string, number, params, today, choices, options['--dry-run'],
                     int(options['--verbosity']))


if __name__ == '__main__':
    main()
