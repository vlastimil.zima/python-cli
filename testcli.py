import sys
from datetime import date
from enum import Enum, unique
from typing import Iterable

__version__ = '0.0.42'

DEFAULT_CONFIG = {
    'string': "Don't panic!",
    'number': 42,
    'params': {},
}


@unique
class Choice(str, Enum):
    ALL = 'all'
    FOO = 'foo'
    BAR = 'bar'
    BAZ = 'baz'


def _do(command: str, label: str, string: str, number: int, params: dict, today: date, choices: Iterable[Choice],
        dry_run: bool, verbosity: int, stdout=None):
    # Juggle around with outputs
    stdout = stdout or sys.stdout
    print("Command: {}".format(command), file=stdout)
    print("Label: {}".format(label), file=stdout)
    assert isinstance(string, str)
    print("String: {}".format(string), file=stdout)
    assert isinstance(number, int)
    print("Number: {}".format(number), file=stdout)
    assert isinstance(params, dict)
    print("Params: {}".format(params), file=stdout)
    assert isinstance(today, date)
    print("Today: {}".format(today), file=stdout)
    assert hasattr(choices, '__iter__')
    assert all(isinstance(c, Choice) for c in choices)
    print("Choices selected: {}".format([c.value for c in choices]), file=stdout)
    assert isinstance(dry_run, bool)
    print("Dry run: {}".format(dry_run), file=stdout)
    assert isinstance(verbosity, int)
    print("Verbosity: {}".format(verbosity), file=stdout)


def create(label: str, string: str, number: int, params: dict, today: date, choices: Iterable[str], dry_run: bool,
           verbosity: int, stdout=None):
    _do('create', label, string, number, params, today, choices, dry_run, verbosity, stdout=stdout)


def drop(label: str, string: str, number: int, params: dict, today: date, choices: Iterable[str], dry_run: bool,
         verbosity: int, stdout=None):
    _do('drop', label, string, number, params, today, choices, dry_run, verbosity, stdout=stdout)
