#!/usr/bin/python3
"""Test client interface."""
import json
from datetime import date, datetime
from typing import Sequence, cast

import click

import testcli


class Date(click.DateTime):
    name = "date"

    def __init__(self):
        super().__init__(['%Y-%m-%d'])

    def convert(self, value, param, ctx):
        result = super().convert(value, param, ctx)
        return result.date()


@click.command()
@click.help_option('-h', '--help')
@click.version_option(testcli.__version__)
@click.argument('action', type=click.Choice(['create', 'drop']))
@click.argument('label')
@click.option('--config', type=click.File('r'), help="Set config file.")
@click.option('--string', help="Set custom string.")
@click.option('--number', type=int, help="Set custom number.")
@click.option('--params', multiple=True, help="Set custom parameters.")
@click.option('--today', type=Date(), default=str(date.today()), help="Set custom today.  [default: today]")
@click.option('--choices', type=click.Choice(list(testcli.Choice)), default=[testcli.Choice.ALL.value],
              show_default=True, multiple=True, help="Set custom choices.")
@click.option('-n', '--dry-run', flag_value=True, default=False, help="Don't actually do anything.")
@click.option('-v', '--verbosity', default=1, show_default=True, help="Set verbosity level in range 0 to 3.")
def main(action: str, label: str, config, string, number, params, today, choices, dry_run, verbosity):
    defaults = testcli.DEFAULT_CONFIG.copy()
    if config:
        defaults.update(json.loads(config.read()))
    string = string or defaults['string']
    number = number or defaults['number']
    if params:
        params = dict(p.split('=', 1) for p in params)
    else:
        params = cast(dict, defaults['params'])

    if testcli.Choice.ALL in choices:
        choices = [c for c in testcli.Choice if c != testcli.Choice.ALL]

    if action == 'create':
        testcli.create(label, string, number, params, today, choices, dry_run, verbosity)
    else:
        assert action == 'drop'
        testcli.drop(label, string, number, params, today, choices, dry_run, verbosity)


if __name__ == '__main__':
    main()
